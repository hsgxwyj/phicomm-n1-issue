v2.27 , v2.28 降级：

```shell
└─[$] <> adb connect 192.168.8.146      
* daemon not running; starting now at tcp:5037
* daemon started successfully
connected to 192.168.8.146:5555
```

```shell
└─[$] <> adb shell
p230:/ $ reboot fastboot
```

```shell
└─[$] <> fastboot devices -l
CAQDBXXXXK0XXXX        fastboot usb:1-3
```


> fastboot flash bootloader bootloader.img

> fastboot flash boot boot.img

> fastboot flash recovery recovery.img


```shell
└─[$] <> fastboot flash bootloader bootloader.img
target reported max download size of 1524629504 bytes
sending 'bootloader' (656 KB)...
OKAY [  0.039s]
writing 'bootloader'...
OKAY [  0.059s]
finished. total time: 0.097s
┌─[hacklog@arch_huangye] - [~/Downloads/N1_V2.19_imgs] - [Thu Sep 06, 19:10]
└─[$] <> fastboot flash boot boot.img
target reported max download size of 1524629504 bytes
sending 'boot' (13858 KB)...
OKAY [  0.639s]
writing 'boot'...
OKAY [  0.725s]
finished. total time: 1.363s
┌─[hacklog@arch_huangye] - [~/Downloads/N1_V2.19_imgs] - [Thu Sep 06, 19:10]
└─[$] <> fastboot flash recovery recovery.img
target reported max download size of 1524629504 bytes
sending 'recovery' (17866 KB)...
OKAY [  0.820s]
writing 'recovery'...
OKAY [  0.916s]
finished. total time: 1.736s
```

气沉丹田，断电.

```shell
└─[$] <> adb connect 192.168.8.146
connected to 192.168.8.146:5555
```

```shell
└─[$] <> adb shell
p230:/ $ reboot update
```
开始从USB刷 Linux